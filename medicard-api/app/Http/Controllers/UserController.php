<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
	public $successStatus = 200;
    //login api
    public function login(){
    	// $request->validate([
    	// 	'username' =>'required|string|unique',
    	// 	'email' =>'required|email|unique',
    	// 	'password' =>'required',
    	// ]);

    	if(Auth::attempt([
    		'email' =>request('email'),
    		'password' =>request('password')
    	]))
    	{
    		$user = Auth::user();
    		$success['token'] = $user->createToken('MediCard')->accessToken;

    		return response()->json(['success' => $success],$this->successStatus);
    	}
    	else{
    		return response()->json(['error'=>'Unauthorized'],401);
    	}

    }

    //allUser api
    public function allUser(){
    	return response()->json([User::all()]);
    }

    //show api

    public function show($id){

    }

    //store/register api
    public function store(Request $request){
    	$validator = Validator::make($request->all(),[
    			'username' =>'required|string',
    		'email' =>'required|email',
    		'password' =>'required',

    	]);

    	if($validator->fails()){
    		return response()->json(['error' => $validator->errors()],401);
    	}

    	// $input = $request->all();
    	// $input['password'] = bcrypt($input['password']);
    	// $user = User::create($input);
    	// $success['token'] = $user->createToken('MediCard')->accessToken;
    	// $success['username'] = $user->username;

    	// return response()->json(['success' =>$success], $this->successStatus);

    	$input = $request->all();  
 $input['password'] = bcrypt($input['password']);
 $user = User::create($input); 
 $success['token'] =  $user->createToken('MediCard')->accessToken;
 return response()->json(['success'=>$success], $this->successStatus); 
    }

    //update api
    public function update(Request $request, $id){

    }

    //destroy api
    public function destroy($id){

    }

}
