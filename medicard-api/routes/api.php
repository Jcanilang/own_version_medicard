<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login','UserController@login');
Route::post('register','UserController@store');
Route::get('users','UserController@allUser');
Route::get('users','UserController@store');
Route::get('users/{id}','UserController@show');
Route::get('users/{id}','UserController@update');
Route::get('users/{id}','UserController@destroy');

// Route::group(['middleware' =>'auth:api'], function()=>{

// 	//UserController
// 	Route::get('users','UserController@allUser');
// 	Route::get('users','UserController@store');
// 	Route::get('users/{id}','UserController@show');
// 	Route::get('users/{id}','UserController@update');
// 	Route::get('users/{id}','UserController@destroy');
// })

